import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPStatement;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.json.JSONObject;

import java.lang.management.ManagementFactory;

public class Main {
    private final static String QUEUE_NAME = "Test";

    public static void main(String[] args) throws Exception {
        new EsperUtils();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.1.56");
        factory.setUsername("user");
        factory.setPassword("user");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        // Eliminar el comentario de abajo si se quieren ver las métricas de rendimiento
        // EsperUtils.addListener(EsperUtils.deployPattern("select * from com.espertech.esper.common.client.metric.StatementMetric;").getStatements()[0]);

        System.out.println(EsperUtils.deployPattern("@public @buseventtype create json schema AnomalyReadingErrors as (eventTypeName string, serialNumber string, dateTime string, volumeM3 double, volumeL double, type string, starts int, batteryLevel int, sleepingTime int, leakingTime int, normalTime int, batteryLevelStr string, timestamp string, anomaly string)").getDeploymentId());

        EPStatement ep = EsperUtils.deployPattern("SELECT a1.serialNumber as serialNumber, a1.dateTime as dateTime, a1.volumeM3 as volumeM3, a1.volumeL as volumeL, a1.type as type, a1.starts as starts, a1.batteryLevel as batteryLevel, a1.sleepingTime as sleepingTime, a1.leakingTime as leakingTime, a1.normalTime as normalTime, a1.batteryLevelStr as batteryLevelStr, current_timestamp().toString() as timestamp, 'Water meter with wrong data' as anomaly FROM AnomalyReadingErrors a1 WHERE a1.starts < 0 or a1.sleepingTime < 0 or a1.leakingTime < 0 or a1.normalTime < 0").getStatements()[0];
        EsperUtils.addListener(ep);

        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        System.out.println(" [*] Waiting for messages...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");

            EsperUtils.sendEvent(message);
        };

        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }

}
