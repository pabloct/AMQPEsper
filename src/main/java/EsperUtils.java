import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.common.client.metric.RuntimeMetric;
import com.espertech.esper.common.client.metric.StatementMetric;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompiler;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.*;

import javax.swing.plaf.nimbus.State;
import java.lang.management.ManagementFactory;

public class EsperUtils {
    private static EPRuntime epRuntime;
    private static EPCompiler epCompiler;
    public static Configuration configuration;

    public EsperUtils() {
        synchronized (this) {
            if (configuration == null) {
                configuration = new Configuration();
                configuration.getRuntime().getMetricsReporting().setEnableMetricsReporting(true);
                configuration.getRuntime().getMetricsReporting().setRuntimeInterval(1000);
                configuration.getRuntime().getMetricsReporting().setStatementInterval(1000);
                configuration.getRuntime().getMetricsReporting().setThreading(true);
                configuration.configure("config.xml");
            }

            if (epCompiler == null) {
                epCompiler = EPCompilerProvider.getCompiler();
            }

            if (epRuntime == null) {
                epRuntime = EPRuntimeProvider.getDefaultRuntime(configuration);
            }
        }
    }

    public static EPRuntime getEpRuntime() {
        synchronized (EsperUtils.class) {
            if (epRuntime == null) {
                throw new RuntimeException("Unable to continue because epRuntime is not defined!");
            }
        }
        return epRuntime;
    }

    public static EPCompiler getEpCompiler() {
        synchronized (EsperUtils.class) {
            if (epCompiler == null) {
                throw new RuntimeException("Unable to continue because epCompiler is not defined!");
            }
        }
        return epCompiler;
    }

    public static EPDeployment deployPattern(String epl) throws EPCompileException, EPDeployException {
        return getEpRuntime().getDeploymentService().deploy(compileNewPattern(epl));
    }

    public static EPCompiled compileNewPattern(String epl) throws EPCompileException {
        CompilerArguments arguments = new CompilerArguments(getEpRuntime().getRuntimePath());
        arguments.setConfiguration(configuration);

        return getEpCompiler().compile(epl, arguments);
    }

    public static void addListener(EPStatement statement) {
        statement.addListener((newComplexEvents, oldComplexEvents, detectedEventPattern, epRuntime) -> {
            if (newComplexEvents != null) {
                String eventPatternName = detectedEventPattern.getEventType().getName();
                Object underlying = newComplexEvents[0].getUnderlying();
                System.out.println("** Complex event '" + eventPatternName + "' detected: " + underlying);

                if (underlying instanceof StatementMetric) {
                    StatementMetric metric = (StatementMetric) underlying;

                    // Se imprime el nombre del statement y el wall time. Consultar documentación de Esper para ver los campos que hay disponibles y qué significa cada uno
                    System.out.println(metric.getStatementName() + " --- " + metric.getWallTime());
                }
            }
        });
    }

    public static void sendEvent(String json) {
        getEpRuntime().getEventService().sendEventJson(json, "AnomalyReadingErrors");
    }
}
